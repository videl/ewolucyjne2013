﻿using CommandLine;
using CommandLine.Text;

namespace PalettePack
{
    class Options
    {
        public Options()
        {
            IterationLimit = 1000;
            PopulationSize = 50;
            MutationRatio = 0.10;
            UseHeuristic = true;
            UseOxCrossover = true;
        }

        [Option("oxCrossover", Required = false, HelpText = "Set this flag to use OX crossover.")] 
        public bool UseOxCrossover { get; set; }

        [Option("mutationRatio", Required = true, HelpText = "Sets fraction of population that will be mutated")] 
        public double MutationRatio { get; set; }

        [Option("populationSize", Required = true, HelpText = "Sets fraction of population size")]
        public int PopulationSize { get; set; }

        [Option("iterationLimit", Required = true, HelpText = "Sets limit of iterations")]
        public int IterationLimit { get; set; }

        [Option("heuristic", Required = false, HelpText = "Computates using fast heuristic")]
        public bool UseHeuristic { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}