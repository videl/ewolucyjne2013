﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using StripPackingLib.Primitives;

namespace PalettePack
{
    class Program
    {
        private static List<Options> _optionList;
        private static DirectoryInfo _outDir;
        private static DirectoryInfo _dir;

        static void Main()
        {
            CreateOptionList();
            InitDirectories();
            
            var debuggingVariable = _dir.FullName;

            foreach (var fileInfo in _dir.EnumerateFiles())
            {
                GatherSolversStatistics(fileInfo);
            }
        }

        private static void GatherSolversStatistics(FileInfo fileInfo)
        {
            var inputData = new InputData(fileInfo.FullName);
            var statList = _optionList.Select(options => SolverExecutor.ExtractStatisticsFromSolution(inputData, options)).ToList();
            List<string> toWrite = Enumerable.Range(0, statList.First().CostHistory.Count())
                .Select(index => string.Format("{0} {1} {2}",
                    statList[0][index],
                    statList[1][index],
                    statList[2][index])).ToList();
            
            WriteToFile(fileInfo, toWrite);
        }

        private static void InitDirectories()
        {
            const string rootDir = "..\\..\\..\\";
            _dir = new DirectoryInfo(rootDir + "PalettePakc\\SampleData\\");
            _outDir = Directory.CreateDirectory(rootDir + "PalettePakc\\OutputData\\");
        }

        private static void CreateOptionList()
        {
            _optionList = new List<Options>
            {
                new Options {UseOxCrossover = false},
                new Options {UseOxCrossover = true},
                new Options {UseHeuristic = true},
            };
        }

        private static void WriteToFile(FileInfo fileInfo, List<string> toWrite)
        {
            var fileName = "out_" + fileInfo.Name;
            var fullFilePath = string.Format("{0}{1}.txt", _outDir, fileName);
            var fileStream = File.OpenWrite(fullFilePath);
            var writer = new StreamWriter(fileStream);

            toWrite.ForEach(writer.WriteLine);
            
            writer.Close();
            fileStream.Close();
        }
    }
}
