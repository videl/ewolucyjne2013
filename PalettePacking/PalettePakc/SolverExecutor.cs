﻿using System.Collections.Generic;
using StripPackingLib.Packers;
using StripPackingLib.Primitives;
using StripPackingLib.Solvers;
using StripPackingLib.Solvers.EvolutionarySolver;
using StripPackingLib.Solvers.EvolutionarySolver.Selectors;

namespace PalettePack
{
    class SolverExecutor
    {
        private static CrossoverOperatorAdapter _crossoverAdapter;
        private static MutationOperatorAdapter _mutationAdapter;
        private static List<EvolutionarySolver.EvolutionaryOperator> _operators;
        private static Options _options;

        public static Statistics ExtractStatisticsFromSolution(InputData inputData, Options options)
        {
            _options = options;
            IPacker packer = new FirstFitStripPacker();

            Statistics statistics;
            if (_options.UseHeuristic)
            {
                statistics = new StraightSolver(options.IterationLimit).Solve(packer, inputData);
            }
            else
            {
                CreateOperators();
                statistics = new EvolutionarySolver(_operators, _options.PopulationSize, _options.IterationLimit)
                    .Solve(packer, inputData);
            }
            return statistics;
        }

        private static void CreateOperators()
        {
            _crossoverAdapter = CreateCrossoverOperatorAdapter();
            _mutationAdapter = CreateMutationOperatorAdapter();

            _operators = new List<EvolutionarySolver.EvolutionaryOperator>
            {
                _crossoverAdapter.ApplyOperator,
                _mutationAdapter.ApplyOperator,
            };
        }

        private static MutationOperatorAdapter CreateMutationOperatorAdapter()
        {
            var mutationAdapter = new MutationOperatorAdapter(new UniformSelector(),
                MutationOperatorHelper.RandomMutation, _options.MutationRatio);
            return mutationAdapter;
        }

        private static CrossoverOperatorAdapter CreateCrossoverOperatorAdapter()
        {
            if (_options.UseOxCrossover)
                return new CrossoverOperatorAdapter(new RouletteSelector(),
                    CrossoverOperatorHelper.OxCrossoverOperator);
            return new CrossoverOperatorAdapter(new RouletteSelector(),
                CrossoverOperatorHelper.PmxCrossoverOperator);
        }
    }
}