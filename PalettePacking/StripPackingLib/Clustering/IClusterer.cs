﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StripPackingLib.Primitives;

namespace StripPackingLib.Clustering
{
    interface IClusterer
    {
        public delegate double Distance(Size s1, Size s2);
     
        // Use some other return type if you find it more convenient.
        public IList<IList<Size>> cluster(IEnumerable<Size> packages, Distance d);
    }
}
