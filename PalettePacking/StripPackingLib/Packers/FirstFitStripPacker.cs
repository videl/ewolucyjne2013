﻿using System.Collections.Generic;
using System.Linq;
using StripPackingLib.Primitives;

namespace StripPackingLib.Packers
{
    public class FirstFitStripPacker : IPacker
    {
        private List<Level> _levels;
        private Level _topLevel;


        public Solution Pack(List<Size> packages, float stripWidth)
        {
            InitializeMembers(stripWidth);

            foreach (var package in packages)
            {
                CreateNewLevelIfPackageDoesntFit(package, stripWidth);
                _topLevel.Insert(package);
            }

            return new Solution(_levels, stripWidth);
        }

        private void InitializeMembers(float stripWidth)
        {
            _levels = new List<Level> {new Level(stripWidth)};
            _topLevel = _levels.Last();
        }

        private void CreateNewLevelIfPackageDoesntFit(Size package, float stripWidth)
        {
            if (CantFitIntoCurrentLevel(package))
            {
                PushNewLevel(stripWidth);
            }
        }

        private bool CantFitIntoCurrentLevel(Size package)
        {
            return !_topLevel.CanInsert(package);
        }

        private void PushNewLevel(float stripWidth)
        {
            _levels.Add(new Level(stripWidth));
            _topLevel = _levels.Last();
        }
    }
}