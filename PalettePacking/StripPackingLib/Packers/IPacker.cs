﻿using System.Collections.Generic;
using StripPackingLib.Primitives;

namespace StripPackingLib.Packers
{
    public interface IPacker
    {
         Solution Pack(List<Size> packages, float stripWidth);
    }
}
