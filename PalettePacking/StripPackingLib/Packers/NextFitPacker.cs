﻿using System.Collections.Generic;
using StripPackingLib.Primitives;

namespace StripPackingLib.Packers
{
    class NextFitPacker : IPacker
    {
        private List<Level> _levels;
        private float _stripWidth;

        public Solution Pack(List<Size> packages, float stripWidth)
        {
            _levels = new List<Level>();
            _stripWidth = stripWidth;

            foreach (var package in packages)
            {
                Level level = FindOrCreateLevel(package);
                level.Insert(package);
            }
            return new Solution(_levels, stripWidth);
        }

        private Level FindOrCreateLevel(Size package)
        {
            foreach (var level in _levels)
            {
                if (level.CanInsert(package))
                    return level;
            }
            var newLevel = new Level(_stripWidth);
            _levels.Add(newLevel);
            return newLevel;
        }
    }
}
