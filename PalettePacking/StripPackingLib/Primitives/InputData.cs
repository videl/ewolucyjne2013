﻿using System;
using System.Collections.Generic;
using System.IO;

namespace StripPackingLib.Primitives
{
    public class InputData
    {
        public float StripWidth { get; private set; }
        public Dictionary<int, Size> PackageIdToSizeMap { get; private set; }

        public InputData()
        {
            StripWidth = 2.0f;
            PackageIdToSizeMap = new Dictionary<int, Size>
            {
                {0, new Size(1, 3, 0)},
                {1, new Size(1, 1, 0)},
                {2, new Size(1, 2, 0)},
                {3, new Size(2, 1, 0)},
            };
        }

        public InputData(string fileName)
        {
            var lines = File.ReadAllLines(fileName);
            ParseStripWidth(lines);
            ParseSizeLines(lines);
        }

        private void ParseStripWidth(string[] lines)
        {
            StripWidth = Int32.Parse(lines[1]);
        }

        private void ParseSizeLines(string[] lines)
        {
            PackageIdToSizeMap = new Dictionary<int, Size>();
            for (int i = 2; i < lines.Length; ++i)
            {
                ParseSingleSizeLine(lines, i);
            }
        }

        private void ParseSingleSizeLine(string[] lines, int i)
        {
            var split = lines[i].Split(' ');
            var size = ParseSize(split);
            var id = ParseId(split);
            PackageIdToSizeMap.Add(id, size);
        }

        private int ParseId(string[] split)
        {
            return Int32.Parse(split[0]);
        }

        private Size ParseSize(string[] split)
        {
            return new Size(Double.Parse(split[1]),
                Double.Parse(split[2]), 0);
        }
    }
}
