﻿using System;

namespace StripPackingLib.Primitives
{
    public class Level
    {
        private float _widthLeft;
        private readonly float _width;
        private float _wasteAccumulated;
        public float Length { get; private set; }
        public float Waste
        {
            get
            {
                return _width*Length - _wasteAccumulated;
            }
        }

        public Level(float maxWidth)
        {
            _widthLeft  = maxWidth;
            _width      = maxWidth;

            _wasteAccumulated = 0.0f;
            Length  = 0.0f;
        }

        public bool CanInsert(Size size)
        {
            return _widthLeft >= size.Width;
        }

        public void Insert(Size size)
        {
            _widthLeft        -= size.Width;
            _wasteAccumulated += size.Width * size.Length;
            Length             = Math.Max(size.Length, Length);
        }
    }
}