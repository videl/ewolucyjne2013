﻿namespace StripPackingLib.Primitives
{
    public class Size
    {
        public Size(double width, double length, double height)
            : this((float) width, (float) length, (float) height)
        {
            
        }

        public Size(float width, float length, float height)
        {
            Width = width;
            Length = length;
            Height = height;
        }

        public float Width { get; set; }
        public float Length { get; set; }
        public float Height { get; set; }
    }
}