﻿using System.Collections.Generic;
using System.Linq;

namespace StripPackingLib.Primitives
{
    public class Solution
    {
        public float Waste      { get; private set; }
        public float WasteRatio { get; private set; }
        public float Length     { get; private set; }
        public float Width      { get; private set; }
        public float Area       { get; private set; }

        public Solution(List<Level> levels, float stripWidth)
        {
            Width   = stripWidth;
            Waste   = levels.Sum(lvl => lvl.Waste);
            Length  = levels.Sum(lvl => lvl.Length);
            Area    = Length * Width;
            WasteRatio = Waste / Area;
        }

        public override string ToString()
        {
//            var builder = new StringBuilder();
//            builder.AppendFormat("Strip length: {0}\n", Length)
//                .AppendFormat("Strip width:  {0}\n", Width)
//                .AppendFormat("Total waste:  {0}\n", Waste)
//                .AppendFormat("Waste ratio:  {0}%\n", WasteRatio);

//            return builder.ToString();
            return Length.ToString();
        }
    }
}
