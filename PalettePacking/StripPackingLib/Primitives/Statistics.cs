﻿using System.Collections.Generic;
using System.Text;

namespace StripPackingLib.Primitives
{
    public class Statistics
    {
        public Statistics()
        {
            CostHistory = new List<double>();
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            foreach (var cost in CostHistory)
            {
                builder.AppendLine(cost.ToString());
            }
            return builder.ToString();
        }

        public List<double> CostHistory { get; set; }

        public double this[int index]
        {
            get { return CostHistory[index]; }
        }
    }
}