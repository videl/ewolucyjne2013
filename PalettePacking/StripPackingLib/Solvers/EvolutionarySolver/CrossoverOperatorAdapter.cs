﻿using System.Collections.Generic;
using System.Linq;
using StripPackingLib.Solvers.EvolutionarySolver.Selectors;

namespace StripPackingLib.Solvers.EvolutionarySolver
{
    public class CrossoverOperatorAdapter
    {
        private readonly ISelector         _selector;
        private readonly CrossoverOperator _operator;
        private Individual[] _selection;

        public CrossoverOperatorAdapter(ISelector selector, CrossoverOperator @operator)
        {
            _selector = selector;
            _operator = @operator;
        }

        public List<Individual> ApplyOperator(IEnumerable<Individual> population)
        {
            var matingPool = SelectMatingPool(population);
            return CalculateOffspring(matingPool);
        }

        private Individual[] SelectMatingPool(IEnumerable<Individual> population)
        {
            var arrayPopulaiton = population as Individual[] ?? population.ToArray();
            var n = arrayPopulaiton.Count();
            var size = (n + n % 1) / 2;
            _selection = _selector.Select(arrayPopulaiton, size).ToArray();
            return arrayPopulaiton;
        }

        private List<Individual> CalculateOffspring(Individual[] arrayPopulaiton)
        {
            var newGeneration = new List<Individual>(arrayPopulaiton.Where(x => !_selection.Contains(x)));
            var n = _selection.Count();
            for (var i = 1; i < n; i += 2)
            {
                var tuple = _operator(_selection[i-1].Permutation, _selection[i].Permutation);
                newGeneration.Add(new Individual { Permutation = tuple.Item1 });
                newGeneration.Add(new Individual { Permutation = tuple.Item2 });
            }
            return newGeneration;
        }
    }
}
