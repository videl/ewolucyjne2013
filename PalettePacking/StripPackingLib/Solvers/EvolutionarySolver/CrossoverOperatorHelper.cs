﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StripPackingLib.Solvers.EvolutionarySolver
{

    public delegate Tuple<int[], int[]> CrossoverOperator(int[] parent1, int[] parent2);

    public static class CrossoverOperatorHelper
    {
        public static Tuple<int[], int[]> PmxCrossoverOperator(int[] parent1, int[] parent2)
        {
            int length = parent1.Length;

            int[] child1 = new int[length];
            int[] child2 = new int[length];

            int[] map1 = Enumerable.Range(0, length + 1).ToArray();
            int[] map2 = Enumerable.Range(0, length + 1).ToArray();

            Random random = new Random();
            int a = random.Next(length);
            int b = random.Next(length);

            for (int i = Math.Min(a, b); i <= Math.Max(a, b); i++)
            {
                map1[parent2[i]] = parent1[i];
                map2[parent1[i]] = parent2[i];

                child1[i] = parent2[i];
                child2[i] = parent1[i];
            }

            for (int i = 0; i < Math.Min(a, b); i++)
            {
                child1[i] = map1[parent1[i]];
                child2[i] = map2[parent2[i]];
            }

            for (int i = Math.Max(a, b) + 1; i < length; i++)
            {
                child1[i] = map1[parent1[i]];
                child2[i] = map2[parent2[i]];
            }

            return new Tuple<int[], int[]>(child1, child2);
        }

        /// <summary>
        /// Uniform Like Crossover
        /// </summary>
        /// <param name="parent1">Parent 1</param>
        /// <param name="parent2">Parent 2</param>
        /// <returns>Child</returns>
        public static int[] UlxCrossoverOperator(int[] parent1, int[] parent2)
        {
            int length = parent1.Length;

            int[] child = new int[length];
            bool[] used = new bool[length + 1];
            used[0] = true;

            // copy when both parent's genes are equal
            for (int i = 0; i < length; i++)
            {
                if (parent1[i] == parent2[i])
                {
                    child[i] = parent1[i];
                    used[parent1[i]] = true;
                }
            }

            // copy from random parent
            Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                if (child[i] == 0)
                {
                    int a = random.NextDouble() < 0.5 ? parent1[i] : parent2[i];
                    if (!used[a])
                    {
                        child[i] = a;
                        used[a] = true;
                    }
                }
            }

            // fill the rest
            IEnumerable<int> notUsed = used.Select((v, i) => i).Where(i => !used[i]).OrderBy(i => random.Next());
            int j = 0;
            foreach (var nu in notUsed)
            {
                for (; j < length; j++)
                {
                    if (child[j] == 0)
                    {
                        child[j] = nu; break;
                    }
                }
            }

            return child;
        }

        /// <summary>
        /// Order Crossover
        /// </summary>
        /// <param name="parent1">Parent 1</param>
        /// <param name="parent2">Parent 2</param>
        /// <returns>Tuple of childs</returns>
        public static Tuple<int[], int[]> OxCrossoverOperator(int[] parent1, int[] parent2)
        {
            int length = parent1.Length;

            int[] child1 = new int[length];
            int[] child2 = new int[length];

            bool[] used1 = new bool[length];
            bool[] used2 = new bool[length];

            Random random = new Random();
            int a = random.Next(length);
            int b = random.Next(length);

            for (int i = Math.Min(a, b); i <= Math.Max(a, b); i++)
            {
                child2[i] = parent1[i];
                used2[parent1[i]] = true;

                child1[i] = parent2[i];
                used1[parent2[i]] = true;
            }

            int j1 = 0, j2 = 0;
            for (int i = 0; i < Math.Min(a, b); i++)
            {
                while (used1[parent1[j1]])
                {
                    j1++;
                }
                used1[parent1[j1]] = true;
                while (used2[parent2[j2]])
                {
                    j2++;
                }
                used2[parent2[j2]] = true;

                child1[i] = parent1[j1];
                child2[i] = parent2[j2];
            }

            for (int i = Math.Max(a, b) + 1; i < length; i++)
            {
                while (used1[parent1[j1]])
                {
                    j1++;
                }
                used1[parent1[j1]] = true;
                while (used2[parent2[j2]])
                {
                    j2++;
                }
                used2[parent2[j2]] = true;

                child1[i] = parent1[j1];
                child2[i] = parent2[j2];
            }

            return new Tuple<int[], int[]>(child1, child2);
        }
    }
}