﻿using System;
using System.Collections.Generic;
using System.Linq;
using StripPackingLib.Packers;
using StripPackingLib.Primitives;

namespace StripPackingLib.Solvers.EvolutionarySolver
{
    public class EvolutionarySolver : ISolver
    {
        private readonly Random       _randomizer;
        private Dictionary<int, Size> _packageIdToSizeMap;
        private IPacker               _packer;
        private float                 _stripWidth;
        private List<Individual>      _population;
        private readonly int          _iterationLimit;
        private readonly Statistics   _statistics;

        public IEnumerable<EvolutionaryOperator> Operators { get; private set; }
        public int PopulationSize { get; private set; }

        public delegate List<Individual> EvolutionaryOperator(List<Individual> population);
        
        public EvolutionarySolver(
            IEnumerable<EvolutionaryOperator> operators,
            int populationSize, int iterationLimit)
        {
            _randomizer = new Random();
            _statistics = new Statistics();

            Operators      = operators;
            PopulationSize = populationSize;
            _iterationLimit = iterationLimit;
        }

        public Statistics Solve(IPacker packer, InputData inputData)
        {
            _stripWidth = inputData.StripWidth;
            _packer = packer;
            _packageIdToSizeMap = inputData.PackageIdToSizeMap;
            _population = CreateRandomPopulation(inputData.PackageIdToSizeMap.Keys.Count);

            return Compute();
        }

        private Statistics Compute()
        {
            EvaluatePopulation();
            for(int i = 0; i < _iterationLimit; ++i)
            {
                ApplyEvolutionaryOperators();
                EvaluatePopulation();
                StoreCurrentIterationStatistics();
            }
            return _statistics;
        }

        private void StoreCurrentIterationStatistics()
        {
            _statistics.CostHistory.Add(BestIndividual().Cost);
        }

        private Individual BestIndividual()
        {
            return _population
                .OrderBy(x => x.Cost)
                .First();
        }

        private void EvaluatePopulation()
        {
            _population.ForEach(x => x.Cost = _packer.Pack(Decode(x.Permutation), _stripWidth).Length);
        }

        private Solution BestIndividualSolution()
        {
            var permutation = _population
                .OrderBy(x => x.Cost)
                .First()
                .Permutation;
            var packages = Decode(permutation);
            return _packer.Pack(packages, _stripWidth);
        }

        private List<Size> Decode(IEnumerable<int> individual)
        {
            return individual.ToList().ConvertAll(x => _packageIdToSizeMap[x]);
        }

        private void ApplyEvolutionaryOperators()
        {
            foreach (var evolutionaryOperator in Operators)
            {
                evolutionaryOperator(_population);
            }
        }

        private List<Individual> CreateRandomPopulation(int singleIndividualSize)
        {
            var population = new List<Individual>();
            for (int i = 0; i < PopulationSize; ++i)
                population.Add( new Individual { Permutation = CreateRandomPermutation(singleIndividualSize) } );
            return population;
        }

        private int[] CreateRandomPermutation(int individualSize)
        {
            int[] individual = Enumerable.Range(0, individualSize).ToArray();
            
            Shuffle(individualSize, ref individual);
            return individual;
        }

        private void Shuffle(int permutataionSize, ref int[] permutation)
        {
            // shuffles permutation using (Fisher-Yates shuffle method)
            for (int i = permutataionSize - 1; i > 0; --i)
            {
                int j = _randomizer.Next(0, i+1);
                var temp = permutation[j];
                permutation[j] = permutation[i];
                permutation[i] = temp;
            }
        }
    }
}
