﻿namespace StripPackingLib.Solvers.EvolutionarySolver
{
    public class Individual
    {
        public int[] Permutation { get; set; }
        public float Cost { get; set; }
    }
}