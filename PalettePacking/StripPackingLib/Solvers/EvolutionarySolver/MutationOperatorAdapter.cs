﻿using System.Collections.Generic;
using System.Linq;
using StripPackingLib.Solvers.EvolutionarySolver.Selectors;

namespace StripPackingLib.Solvers.EvolutionarySolver
{
    public class MutationOperatorAdapter
    {
        private readonly ISelector        _selector;
        private readonly MutationOperator _operator;
        private readonly double           _mutationRatio;
        private          Individual[]     _selection;

        public MutationOperatorAdapter(ISelector selector, MutationOperator @operator, double mutationRatio)
        {
            _selector = selector;
            _operator = @operator;
            _mutationRatio = mutationRatio;
        }

        public List<Individual> ApplyOperator(IEnumerable<Individual> population)
        {
            var matingPool = SelectMatingPool(population);
            return CalculateOffspring(matingPool);
        }

        private Individual[] SelectMatingPool(IEnumerable<Individual> population)
        {
            var arrayPopulaiton = population as Individual[] ?? population.ToArray();
            var n = arrayPopulaiton.Count();
            var size = (int) (_mutationRatio * n);

            _selection = _selector.Select(arrayPopulaiton, size).ToArray();
            return arrayPopulaiton;
        }

        private List<Individual> CalculateOffspring(Individual[] arrayPopulation)
        {
            var nextGeneration = new List<Individual>(arrayPopulation.Where(x => !_selection.Contains(x)));
            nextGeneration.AddRange(
                _selection
                    .Select(individual => _operator(individual.Permutation))
                    .Select(mutatedIndividual => new Individual {Permutation = mutatedIndividual}));

            return nextGeneration;
        }
    }
}
