﻿using System;

namespace StripPackingLib.Solvers.EvolutionarySolver
{
    public delegate int[] MutationOperator(int[] individual);
    public static class MutationOperatorHelper
    {
        public static int[] RandomMutation(int[] individual)
        {
            Random random = new Random();

            int a = random.Next(individual.Length);
            int b = random.Next(individual.Length);

            int tmp = individual[a];
            individual[a] = individual[b];
            individual[b] = tmp;

            return individual;
        }
    }
}