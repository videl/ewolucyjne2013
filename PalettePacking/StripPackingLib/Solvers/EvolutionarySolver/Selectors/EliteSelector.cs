﻿using System.Collections.Generic;
using System.Linq;

namespace StripPackingLib.Solvers.EvolutionarySolver.Selectors
{
    public class EliteSelector : ISelector
    {
        public IEnumerable<Individual> Select(IEnumerable<Individual> population, int selectionSize)
        {
            return population.OrderBy(x => x.Cost).Take(selectionSize);
        }
    }
}
