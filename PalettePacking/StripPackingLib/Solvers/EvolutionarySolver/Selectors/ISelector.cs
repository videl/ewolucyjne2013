﻿using System.Collections.Generic;

namespace StripPackingLib.Solvers.EvolutionarySolver.Selectors
{
    public interface ISelector
    {
        IEnumerable<Individual> Select(IEnumerable<Individual> population, int selectionSize);
    }
}