﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StripPackingLib.Solvers.EvolutionarySolver.Selectors
{
    public class RouletteSelector : ISelector
    {
        public IEnumerable<Individual> Select(IEnumerable<Individual> population, int selectionSize)
        {
            var individuals = population as Individual[] ?? population.ToArray();
            var n = individuals.Count();
            var rnd = new Random();

            // see http://www.ii.uni.wroc.pl/~lipinski/pl/ea/lecture03.pdf page 6
            var min = individuals.Min(x => x.Cost);           // Fmin
            var max = individuals.Max(x => x.Cost);           // Fmax
            var avg = individuals.Sum(x => x.Cost) - n * min; // denominator of fitness formula
            return individuals.OrderBy(x => rnd.NextDouble() < (x.Cost - min) / avg ).Take(selectionSize);
        }
    }
}
