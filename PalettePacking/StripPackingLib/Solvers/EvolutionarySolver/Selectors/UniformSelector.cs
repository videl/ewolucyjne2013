﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StripPackingLib.Solvers.EvolutionarySolver.Selectors
{
    public class UniformSelector : ISelector
    {
        public IEnumerable<Individual> Select(IEnumerable<Individual> population, int selectionSize)
        {
            var individuals = population as Individual[] ?? population.ToArray();
            var n = individuals.Count();
            var rnd = new Random();
            return individuals.OrderBy(x => rnd.Next(0,n-1)).Take(selectionSize);
        }
    }
}
