﻿using StripPackingLib.Packers;
using StripPackingLib.Primitives;

namespace StripPackingLib.Solvers
{
    public interface ISolver
    {
        Statistics Solve(IPacker packer, InputData inputData);
    }
}