﻿using System.Linq;
using StripPackingLib.Packers;
using StripPackingLib.Primitives;

namespace StripPackingLib.Solvers
{
    public class StraightSolver : ISolver
    {
        private readonly int _multiplicity;

        public StraightSolver(int multiplicity)
        {
            _multiplicity = multiplicity;
        }

        public Statistics Solve(IPacker packer, InputData inputData)
        {
            var result = packer.Pack(inputData.PackageIdToSizeMap.Values.ToList(), inputData.StripWidth).Length;

            return new Statistics
            {
                CostHistory =
                    Enumerable.Range(0, _multiplicity)
                        .Select(_ => (double)result)
                        .ToList()
            };
        }
    }
}
